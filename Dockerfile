FROM ubuntu:22.04
RUN apt-get update && apt-get upgrade -y
RUN apt-get install python3-pip python3-dev gcc-avr binutils-avr avr-libc make avrdude -y
RUN pip3 install --upgrade pip && pip3 install intelhex
