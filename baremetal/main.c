// compile with avr-gcc
#define F_CPU 16000000
#include <avr/io.h>
#include <util/delay.h>
#define MS_DELAY 1000

int main()
{
  // set pin 13 to output mode.
  DDRB |= _BV(DDB5);
  while(1) {
      // set pin 13 to HIGH
      PORTB |= _BV(PORTB5);
      _delay_ms(MS_DELAY);
      // set pin 13 to LOW
      PORTB &= ~_BV(PORTB5);
      _delay_ms(MS_DELAY);
  }
}
